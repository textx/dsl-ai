import os
import time
import random
import json
from kafka import KafkaProducer
KAFKA_BOOTSTRAP_SERVERS = os.environ.get("KAFKA_BOOTSTRAP_SERVERS", "localhost:29092")
KAFKA_TOPIC_TEST = os.environ.get("KAFKA_TOPIC_TEST", "test")
KAFKA_API_VERSION = os.environ.get("KAFKA_API_VERSION", "7.4.0")
producer = KafkaProducer(
    bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS],
    api_version=KAFKA_API_VERSION,
)

while True:
    try:
        input_event = input('Enter event: ')
        if input_event == 'x':
            break
        producer.send(
            KAFKA_TOPIC_TEST,
            json.dumps({"message": input_event}).encode("utf-8"),
        )
    except Exception:
        print('Invalid input')
producer.flush()

