import uvicorn
from pathlib import Path
from textx import metamodel_from_file
from servers.machine.state_machine import StateMachine

def start_api_server():
    uvicorn.run(
        "servers.backend.app:app",
        host="0.0.0.0",
        port=8000,
        log_level="debug",
        reload=True,
    )

if __name__ == "__main__":
    start_api_server()
