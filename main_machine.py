from pathlib import Path
from textx import metamodel_from_file
from servers.machine.state_machine import StateMachine

def start_machine():
    meta_model = metamodel_from_file(Path(__file__).parent / 'servers' / 'machine' / 'state_machine.tx' )
    model = meta_model.model_from_file(Path(__file__).parent / 'servers' / 'machine' / 'iris.robo' )
    state_machine = StateMachine(model)
    state_machine.interpret()

if __name__ == "__main__":
    start_machine()
