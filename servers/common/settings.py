from pydantic import BaseSettings
from pathlib import Path
import os


class Settings(BaseSettings):
    kafka_bootstrap_servers:str = "localhost:29092"
    kafka_topic:str
    kafka_api_version:str = "7.4.0"

    class Config:
        env_file = Path(__file__).resolve().parent / 'envs' / f'{os.getenv("STAGE") if os.getenv("STAGE") is not None else ""}.env'

common_settings = Settings()
