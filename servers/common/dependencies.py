from kafka import KafkaProducer, KafkaConsumer
from servers.common.settings import common_settings

def get_producer():
    return KafkaProducer(
        bootstrap_servers=[common_settings.kafka_bootstrap_servers],
        api_version=common_settings.kafka_api_version,
    )

def get_consumer():
    return KafkaConsumer(
            common_settings.kafka_topic,
            bootstrap_servers=[common_settings.kafka_bootstrap_servers],
            api_version=common_settings.kafka_api_version,
            auto_offset_reset="latest",
            enable_auto_commit=True,
        )
