from typing import List, Union

from pydantic import BaseModel


class Item(BaseModel):
    sepal_length: int
    sepal_width: int
    petal_length: int
    petal_width: int
