import numpy as np 
import pandas as pd
from sklearn import datasets
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics
import pickle

from pathlib import Path

def create_model():
    iris = datasets.load_iris()
    df = pd.DataFrame(data=iris.data, columns=iris.feature_names)
    df['Species'] = pd.Series(iris.target)

    df.rename(columns={'sepal length (cm)':'SepalLengthCm','sepal width (cm)':'SepalWidthCm','petal length (cm)':'PetalLengthCm','petal width (cm)':'PetalWidthCm'},inplace=True)

    train,test = train_test_split(df,test_size=0.3)
    train_X = train[['SepalLengthCm','SepalWidthCm','PetalLengthCm','PetalWidthCm']]
    test_X = test[['SepalLengthCm','SepalWidthCm','PetalLengthCm','PetalWidthCm']]
    train_y=train['Species']
    test_y=test['Species']

    model = LogisticRegression()
    model.fit(train_X,train_y)
    prediction = model.predict(test_X)
    metrics.accuracy_score(prediction,test_y)

    pickle_output_location = Path(__file__).resolve().parent / 'output_data'
    pickle_output_location.mkdir()

    with open(pickle_output_location / "model.pkl", "wb") as f:
        pickle.dump(model, f)