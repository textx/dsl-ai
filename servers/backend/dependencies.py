from pathlib import Path
from kafka import KafkaProducer
import pickle

def get_model():
    pickle_location = Path(__file__).parent / 'output_data'
    with open(pickle_location / "model.pkl", "rb") as f:
        model_pkl_load = pickle.load(f)
    yield model_pkl_load

