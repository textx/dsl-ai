from fastapi import FastAPI,Request, Depends
from pathlib import Path
from servers.backend.model_creation import create_model
from servers.backend.schemas import Item
from servers.backend.dependencies import get_model
from servers.common.dependencies import get_producer
from servers.common.settings import common_settings
import numpy as np
import os
import json
from kafka import KafkaProducer
import pickle

app = FastAPI()

@app.on_event('startup')
def startup():
    pickle_location = Path(__file__).parent / 'output_data'
    if not pickle_location.is_dir():
        create_model()    

@app.get('/')
def read_root(request: Request):
    return {
        'status' : 'SUCCESS'
    }

@app.post('/predict')
def predict(request: Request, item: Item, model = Depends(get_model), producer = Depends(get_producer)):
    input_data= np.array([[item.sepal_length,item.sepal_width,item.petal_length,item.petal_width]])
    prediction_for_input = model.predict(input_data)
    producer.send(
            common_settings.kafka_topic,
            json.dumps({"message": int(prediction_for_input[0]) + 1, "type":"predict","user":"123"}).encode("utf-8"),
        )
    return {
        'status' : str(prediction_for_input[0])
    }