import sys
import os
import json
from kafka import KafkaConsumer
from textx import metamodel_from_file

from servers.common.settings import common_settings
from servers.common.dependencies import get_consumer , get_producer

class StateMachine(object):
    """
    StateMachine model interpreter.
    """
    def __init__(self, model):
        self.model = model

        self.current_state = None

        # First state is initial
        self.initial_state = model.states[0]

        # We are in the initial state at the beginning
        self.set_state(self.initial_state)

    def set_state(self, state):
        if self.current_state is None or state is not self.current_state:
            print("Transition to state ", state.name)
            # Trigger state actions
            for action in state.actions:
                group_finder = [idx for idx,objx in enumerate(self.model.commandGroupBlocks) if action == objx]
                if len(group_finder) > 0:
                    for grpCommand in self.model.commandGroupBlocks[group_finder[0]].groupCommands:
                        print("Executing group action ", grpCommand.name)                                            
                else:
                    print("Executing individual action ", action.name)
            
            self.current_state = state
        
        # sleep 3s to emulate actions
        if state.emit is not None:
            print('Emitting',state.emit.event.name)
            self.set_state(state.emit.to_state)
            producer = get_producer()
            producer.send(
                common_settings.kafka_topic,
                json.dumps({"message": f"Finished {state.name}", "type":"result","user":"123"}).encode("utf-8"),
            )



    def event(self, event):        
        for transition in self.current_state.transitions:
            if transition.event is event:
                self.set_state(transition.to_state)
                return

    def print_menu(self):
        print("Current state: ", self.current_state.name)
        print("Choose event:")
        for idx, event in enumerate(self.model.events):
            print(idx + 1, '-', event.name, event.code)       
        print('q - quit')

    def interpret(self):
        """
        Main interpreter loop.
        """
        self.print_menu()
        consumer = get_consumer()
        try:
            for message in consumer:
                record = json.loads(message.value)
                if record['type'] == 'predict':
                    print("Receiving event: ",record['message'])
                    event = str(record['message'])             
                    if event == 'q':  
                        consumer.close()                  
                        return
                    event = int(event)                    
                    event = self.model.events[event - 1]
                    self.event(event)
                    self.print_menu()
        except Exception:
            consumer.close()
            print('Invalid input')

if __name__ == '__main__':
    this_folder = os.path.dirname(__file__)
    if len(sys.argv) != 2:
        print("Usage: python {} <model>".format(sys.argv[0]))
    else:
        meta_model = metamodel_from_file(os.path.join(this_folder, 'state_machine.tx'))
        model = meta_model.model_from_file(sys.argv[1])
        state_machine = StateMachine(model)
        state_machine.interpret()